package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDao;
import by.epam.task1.exception.DaoException;
import by.epam.task1.domain.Author;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 11/02/2016.
 */
@Named
public class AuthorDaoImpl implements AuthorDao {
    @Inject
    private DataSource dataSource;

    private static final String SQL_AUTHOR_CREATE = "INSERT INTO authors(author_name) VALUES(?)";
    private static final String SQL_AUTHOR_READ = "SELECT author_name, expired " +
            "FROM authors WHERE author_id = ?";
    private static final String SQL_AUTHOR_UPDATE = "UPDATE authors SET author_name = ? " +
            "WHERE author_id = ?";
    private static final String SQL_AUTHOR_DELETE = "DELETE FROM authors WHERE author_id = ?";
    private static final String SQL_AUTHOR_READ_LIST_ALL = "SELECT author_id, author_name " +
            "FROM authors WHERE expired > CURRENT_TIMESTAMP OR expired IS NULL ORDER BY author_name";
    private static final String SQL_AUTHOR_READ_LIST = "SELECT * FROM (SELECT a.*, ROWNUM rn " +
            "FROM (SELECT author_id, author_name, expired FROM authors ORDER BY author_name) a " +
            "WHERE ROWNUM < ?) WHERE rn >= ?";
    private static final String SQL_AUTHOR_READ_BY_NEWS = "SELECT author_id, author_name " +
            "FROM authors WHERE author_id = (SELECT author_id FROM news_authors WHERE news_id = ?)";
    private static final String SQL_AUTHOR_MAKE_EXPIRED = "UPDATE authors SET expired = ? " +
            "WHERE author_id = ?";

    private static final String AUTHOR_ID_FIELD_NAME = "author_id";

    public Long create(Author author) throws DaoException {
        String[] generatedKeys = { AUTHOR_ID_FIELD_NAME };
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                SQL_AUTHOR_CREATE, generatedKeys)) {
            statement.setNString(1, author.getAuthorName());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                Long authorId = null;
                if (resultSet.next()) {
                    authorId = resultSet.getLong(1);
                }
                return authorId;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Author read(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_AUTHOR_READ)) {
            statement.setLong(1, authorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                Author author = null;
                if (resultSet.next()) {
                    author = new Author();
                    author.setAuthorId(authorId);
                    author.setAuthorName(resultSet.getNString(1));
                    author.setExpired(resultSet.getTimestamp(2));
                }
                return author;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void update(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_AUTHOR_UPDATE)) {
            statement.setNString(1, author.getAuthorName());
            statement.setLong(2, author.getAuthorId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void delete(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_AUTHOR_DELETE)) {
            statement.setLong(1, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Author> readListAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_AUTHOR_READ_LIST_ALL)) {
            List<Author> authors = new ArrayList<>();
            Author author;
            while (resultSet.next()) {
                author = new Author();
                author.setAuthorId(resultSet.getLong(1));
                author.setAuthorName(resultSet.getNString(2));
                authors.add(author);
            }
            return authors;
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Author> readList(long from, long to) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_AUTHOR_READ_LIST)) {
            statement.setLong(1, to);
            statement.setLong(2, from);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Author> authors = new ArrayList<>();
                Author author;
                while (resultSet.next()) {
                    author = new Author();
                    author.setAuthorId(resultSet.getLong(1));
                    author.setAuthorName(resultSet.getNString(2));
                    authors.add(author);
                }
                return authors;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Author readByNews(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_AUTHOR_READ_BY_NEWS)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                Author author = null;
                if (resultSet.next()) {
                    author = new Author();
                    author.setAuthorId(resultSet.getLong(1));
                    author.setAuthorName(resultSet.getNString(2));
                }
                return author;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void makeExpired(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_AUTHOR_MAKE_EXPIRED)) {
            statement.setTimestamp(1, new Timestamp(author.getExpired().getTime()));
            statement.setLong(2, author.getAuthorId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
