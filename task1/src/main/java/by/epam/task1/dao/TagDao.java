package by.epam.task1.dao;

import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DaoException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */

/**
 * This interface provides methods for working with database
 * for instance <code>Tag</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Tag
 * @see by.epam.task1.dao.GenericCRUDDao
 */
public interface TagDao extends GenericCRUDDao<Tag> {
    /**
     * Returns a list of all tags.
     *
     * @return a list of all tags
     * @throws DaoException if SQLException is thrown
     */
    List<Tag> readListAll() throws DaoException;

    /**
     * Returns a list of tags from value <code>from</code>
     * to value <code>to</code>.
     *
     * @param from number from which tags are to be returned
     * @param to number to which tags are to be returned
     * @return a list of tags
     * @throws DaoException if SQLException is thrown
     */
    List<Tag> readList(long from, long to) throws DaoException;

    /**
     * Returns a list of tags by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which tags are to be returned
     * @return a list of tags
     * @throws DaoException if SQLException is thrown
     */
    List<Tag> readListByNews(Long newsId) throws DaoException;

    /**
     * Removes binding between tag with id <code>tagId</code> and all news.
     *
     * @param tagId id of tag
     * @throws DaoException if SQLException is thrown
     */
    void removeNewsBinding(Long tagId) throws DaoException;
}
