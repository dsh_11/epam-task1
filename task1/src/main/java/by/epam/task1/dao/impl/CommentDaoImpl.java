package by.epam.task1.dao.impl;

import by.epam.task1.dao.CommentDao;
import by.epam.task1.exception.DaoException;
import by.epam.task1.domain.Comment;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 11/02/2016.
 */
@Named
public class CommentDaoImpl implements CommentDao {
    @Inject
    private DataSource dataSource;

    private static final String SQL_COMMENT_CREATE = "INSERT INTO comments(news_id, " +
            "comment_text, creation_date) VALUES(?, ?, ?)";
    private static final String SQL_COMMENT_READ = "SELECT news_id, comment_text, " +
            "creation_date FROM comments WHERE comment_id = ?";
    private static final String SQL_COMMENT_UPDATE = "UPDATE comments SET comment_text = ? " +
            "WHERE comment_id = ?";
    private static final String SQL_COMMENT_DELETE = "DELETE FROM comments WHERE comment_id = ?";
    private static final String SQL_COMMENT_READ_LIST_BY_NEWS = "SELECT comment_id, comment_text, " +
            "creation_date FROM comments WHERE news_id = ? ORDER BY creation_date DESC";
    private static final String SQL_COMMENT_DELETE_BY_NEWS = "DELETE FROM comments WHERE news_id = ?";

    private static final String COMMENT_ID_FIELD_NAME = "comment_id";

    public Long create(Comment comment) throws DaoException {
        String[] generatedKeys = { COMMENT_ID_FIELD_NAME };
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                SQL_COMMENT_CREATE, generatedKeys)) {
            statement.setLong(1, comment.getNewsId());
            statement.setNString(2, comment.getCommentText());
            statement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()));
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                Long commentId = null;
                if (resultSet.next()) {
                    commentId = resultSet.getLong(1);
                }
                return commentId;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Comment read(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_COMMENT_READ)) {
            statement.setLong(1, commentId);
            try (ResultSet resultSet = statement.executeQuery()) {
                Comment comment = null;
                if (resultSet.next()) {
                    comment = new Comment();
                    comment.setCommentId(commentId);
                    comment.setNewsId(resultSet.getLong(1));
                    comment.setCommentText(resultSet.getNString(2));
                    comment.setCreationDate(resultSet.getTimestamp(3));
                }
                return comment;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void update(Comment comment) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_COMMENT_UPDATE)) {
            statement.setNString(1, comment.getCommentText());
            statement.setLong(2, comment.getCommentId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void delete(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_COMMENT_DELETE)) {
            statement.setLong(1, commentId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Comment> readListByNews(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_COMMENT_READ_LIST_BY_NEWS)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Comment> comments = new ArrayList<>();
                Comment comment;
                while (resultSet.next()) {
                    comment = new Comment();
                    comment.setNewsId(newsId);
                    comment.setCommentId(resultSet.getLong(1));
                    comment.setCommentText(resultSet.getNString(2));
                    comment.setCreationDate(resultSet.getTimestamp(3));
                    comments.add(comment);
                }
                return comments;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void deleteByNews(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_COMMENT_DELETE_BY_NEWS)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
