package by.epam.task1.dao;

import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DaoException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */

/**
 * This interface provides methods for working with database
 * for instance <code>News</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.News
 * @see by.epam.task1.dao.GenericCRUDDao
 */
public interface NewsDao extends GenericCRUDDao<News> {
    /**
     * Returns a list of news from value <code>from</code>
     * to value <code>to</code>.
     *
     * @param from number from which news are to be returned
     * @param to number to which news are to be returned
     * @return a list of news
     * @throws DaoException if SQLException is thrown
     */
    List<News> readList(long from, long to) throws DaoException;

    /**
     * Returns a list of news from value <code>from</code>
     * to value <code>to</code> ordered by the number of comments.
     *
     * @param from number from which news are to be returned
     * @param to number to which news are to be returned
     * @return a list of news
     * @throws DaoException if SQLException is thrown
     */
    List<News> readListOrderByCommentsNumber(long from, long to) throws DaoException;

    /**
     * Returns number of all news.
     *
     * @return number of al news
     * @throws DaoException if SQLException is thrown
     */
    long getNumber() throws DaoException;

    /**
     * Returns a list of news satisfying <code>searchCriteria</code>
     * from value <code>from</code> to value <code>to</code>.
     *
     * @param searchCriteria search criteria by which news are to be searched
     * @param from number from which news are to be returned
     * @param to number to which news are to be returned
     * @return a list of news
     * @throws DaoException if SQLException is thrown
     */
    List<News> search(SearchCriteriaTO searchCriteria, long from, long to) throws DaoException;

    /**
     * Binds news with id <code>newsId</code> with tags list <code>tags</code>.
     *
     * @param newsId id of news which should be bound with tag
     * @param tagIds ids of tags which should be bound with news
     * @throws DaoException if SQLException is thrown
     */
    void addTagsBinding(Long newsId, List<Long> tagIds) throws DaoException;

    /**
     * Binds news with id <code>newsId</code> with author
     * with id <code>authorId</code>.
     *
     * @param newsId id of news which should be bound with author
     * @param authorId id of author which should be bound with news
     * @throws DaoException if SQLException is thrown
     */
    void addAuthorBinding(Long newsId, Long authorId) throws DaoException;

    /**
     * Removes binding between news with id <code>newsId</code> and tags
     * list <code>tags</code>.
     *
     * @param newsId id of news which should be unbound from tags
     * @param tagIds ids of tags which should be unbound from news
     * @throws DaoException if SQLException is thrown
     */
    void removeTagsBinding(Long newsId, List<Long> tagIds) throws DaoException;

    /**
     * Removes binding between news with id <code>newsId</code> and all tags.
     *
     * @param newsId id of news which should be unbound from tags
     * @throws DaoException if SQLException is thrown
     */
    void removeTagBinding(Long newsId) throws DaoException;

    /**
     * Removes binding between news with id <code>newsId</code> and author.
     *
     * @param newsId id of news which should be unbound from author
     * @throws DaoException if SQLException is thrown
     */
    void removeAuthorBinding(Long newsId) throws DaoException;
}
