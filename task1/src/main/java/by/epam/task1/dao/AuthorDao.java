package by.epam.task1.dao;

import by.epam.task1.domain.Author;
import by.epam.task1.exception.DaoException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */

/**
 * This interface provides methods for working with database
 * for instance <code>Author</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Author
 * @see by.epam.task1.dao.GenericCRUDDao
 */
public interface AuthorDao extends GenericCRUDDao<Author> {
    /**
     * Returns a list of all authors.
     *
     * @return a list of all authors
     * @throws DaoException if SQLException is thrown
     */
    List<Author> readListAll() throws DaoException;

    /**
     * Returns a list of authors from value <code>from</code>
     * to value <code>to</code>.
     *
     * @param from number from which authors are to be returned
     * @param to number to which authors are to be returned
     * @return a list of authors which is to be returned
     * @throws DaoException if SQLException is thrown
     */
    List<Author> readList(long from, long to) throws DaoException;

    /**
     * Returns an author by news with id <code>newsId</code>.
     *
     * @param newsId id of news
     * @return an author by news id <code>newsId</code>
     * @throws DaoException if SQLException is thrown
     */
    Author readByNews(Long newsId) throws DaoException;

    /**
     * Sets value for author's expired field.
     *
     * @param author author which expired field is to be set
     *               to the value specified in author's expired field
     * @throws DaoException if SQLException is thrown
     */
    void makeExpired(Author author) throws DaoException;
}
