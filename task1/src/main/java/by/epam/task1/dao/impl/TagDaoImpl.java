package by.epam.task1.dao.impl;

import by.epam.task1.exception.DaoException;
import by.epam.task1.dao.TagDao;
import by.epam.task1.domain.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 11/02/2016.
 */
@Named
public class TagDaoImpl implements TagDao {
    @Inject
    private DataSource dataSource;

    private static final String SQL_TAG_CREATE = "INSERT INTO tags(tag_name) VALUES(?)";
    private static final String SQL_TAG_READ = "SELECT tag_name FROM tags WHERE tag_id = ?";
    private static final String SQL_TAG_UPDATE = "UPDATE tags SET tag_name = ? WHERE tag_id = ?";
    private static final String SQL_TAG_DELETE = "DELETE FROM tags WHERE tag_id = ?";
    private static final String SQL_TAG_READ_LIST_ALL = "SELECT tag_id, tag_name FROM tags " +
            "ORDER BY tag_name";
    private static final String SQL_TAG_READ_LIST = "SELECT * FROM (SELECT a.*, " +
            "ROWNUM rn FROM (SELECT tag_id, tag_name FROM tags ORDER BY tag_name) a " +
            "WHERE ROWNUM < ?) WHERE rn >= ?";
    private static final String SQL_TAG_READ_LIST_BY_NEWS = "SELECT tag_id, tag_name FROM tags " +
            "WHERE tag_id IN (SELECT tag_id FROM news_tags WHERE news_id = ?) ORDER BY tag_name";
    private static final String SQL_TAG_REMOVE_NEWS_BINDING = "DELETE FROM news_tags WHERE tag_id = ?";

    private static final String TAG_ID_FIELD_NAME = "tag_id";

    public Long create(Tag tag) throws DaoException {
        String[] generatedKeys = { TAG_ID_FIELD_NAME };
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                SQL_TAG_CREATE, generatedKeys)) {
            statement.setNString(1, tag.getTagName());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                Long tagId = null;
                if (resultSet.next()) {
                    tagId = resultSet.getLong(1);
                }
                return tagId;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Tag read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_TAG_READ)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                Tag tag = null;
                if (resultSet.next()) {
                    tag = new Tag();
                    tag.setTagId(id);
                    tag.setTagName(resultSet.getNString(1));
                }
                return tag;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void update(Tag tag) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_TAG_UPDATE)) {
            statement.setNString(1, tag.getTagName());
            statement.setLong(2, tag.getTagId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_TAG_DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Tag> readListAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_TAG_READ_LIST_ALL)) {
            List<Tag> tags = new ArrayList<>();
            Tag tag;
            while (resultSet.next()) {
                tag = new Tag();
                tag.setTagId(resultSet.getLong(1));
                tag.setTagName(resultSet.getNString(2));
                tags.add(tag);
            }
            return tags;
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Tag> readList(long from, long to) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_TAG_READ_LIST)) {
            statement.setLong(1, to);
            statement.setLong(2, from);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Tag> tags = new ArrayList<>();
                Tag tag;
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setTagId(resultSet.getLong(1));
                    tag.setTagName(resultSet.getNString(2));
                    tags.add(tag);
                }
                return tags;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Tag> readListByNews(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_TAG_READ_LIST_BY_NEWS)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Tag> tags = new ArrayList<>();
                Tag tag;
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setTagId(resultSet.getLong(1));
                    tag.setTagName(resultSet.getNString(2));
                    tags.add(tag);
                }
                return tags;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void removeNewsBinding(Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_TAG_REMOVE_NEWS_BINDING)) {
            statement.setLong(1, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
