package by.epam.task1.dao.impl;

import by.epam.task1.exception.DaoException;
import by.epam.task1.dao.NewsDao;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.util.SearchCriteriaHelper;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Statement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
@Named
public class NewsDaoImpl implements NewsDao {
    @Inject
    private DataSource dataSource;

    private static final String SQL_NEWS_CREATE = "INSERT INTO news(title, " +
            "short_text, full_text, creation_date, modification_date) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_NEWS_READ = "SELECT title, short_text, full_text, " +
            "creation_date, modification_date FROM news WHERE news_id = ?";
    private static final String SQL_NEWS_UPDATE = "UPDATE news SET title = ?, " +
            "short_text = ?, full_text = ?, modification_date = ? WHERE news_id = ?";
    private static final String SQL_NEWS_DELETE = "DELETE FROM news WHERE news_id = ?";
    private static final String SQL_NEWS_GET_LIST = "SELECT * FROM (SELECT a.*, ROWNUM rn " +
            "FROM (SELECT news_id, title, short_text, creation_date, modification_date " +
            "FROM news ORDER BY creation_date DESC) a WHERE ROWNUM < ?) WHERE rn >= ?";
    private static final String SQL_NEWS_GET_LIST_MOST_COMMENTED = "SELECT * FROM (SELECT a.*, " +
            "ROWNUM rn FROM (SELECT news_id, title, short_text, creation_date, modification_date " +
            "FROM news ORDER BY (SELECT COUNT(*) FROM comments WHERE comments.news_id = news.news_id) " +
            "DESC) a WHERE ROWNUM < ?) WHERE rn >= ?";
    private static final String SQL_NEWS_GET_NUMBER = "SELECT COUNT(*) FROM news";
    private static final String SQL_NEWS_ADD_TAG_BINDING = "INSERT INTO news_tags(news_id, tag_id) " +
            "VALUES(?, ?)";
    private static final String SQL_NEWS_ADD_AUTHOR_BINDING = "INSERT INTO news_authors(news_id, author_id) " +
            "VALUES(?, ?)";
    private static final String SQL_NEWS_REMOVE_TAG_BINDING = "DELETE FROM news_tags WHERE news_id = ? " +
            "AND tag_id = ?";
    private static final String SQL_NEWS_REMOVE_TAG_BINDING_BY_NEWS = "DELETE FROM news_tags WHERE news_id = ?";
    private static final String SQL_NEWS_REMOVE_AUTHOR_BINDING = "DELETE FROM news_authors WHERE news_id = ?";

    private static final String NEWS_ID_FIELD_NAME = "news_id";

    public Long create(News news) throws DaoException {
        String[] generatedKeys = { NEWS_ID_FIELD_NAME };
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                SQL_NEWS_CREATE, generatedKeys)) {
            statement.setNString(1, news.getTitle());
            statement.setNString(2, news.getShortText());
            statement.setNString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            statement.setDate(5, new Date(news.getModificationDate().getTime()));
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                Long newsId = null;
                if (resultSet.next()) {
                    newsId = resultSet.getLong(1);
                }
                return newsId;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public News read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_NEWS_READ)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                News news = null;
                if (resultSet.next()) {
                    news = new News();
                    news.setNewsId(id);
                    news.setTitle(resultSet.getNString(1));
                    news.setShortText(resultSet.getNString(2));
                    news.setFullText(resultSet.getNString(3));
                    news.setCreationDate(resultSet.getTimestamp(4));
                    news.setModificationDate(resultSet.getDate(5));
                }
                return news;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void update(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_NEWS_UPDATE)) {
            statement.setNString(1, news.getTitle());
            statement.setNString(2, news.getShortText());
            statement.setNString(3, news.getFullText());
            statement.setDate(4, new Date(news.getModificationDate().getTime()));
            statement.setLong(5, news.getNewsId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_NEWS_DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> readList(long from, long to) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_NEWS_GET_LIST)) {
            statement.setLong(1, to);
            statement.setLong(2, from);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                News news;
                while (resultSet.next()) {
                    news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setCreationDate(resultSet.getTimestamp(4));
                    news.setModificationDate(resultSet.getDate(5));
                    newsList.add(news);
                }
                return newsList;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> readListOrderByCommentsNumber(long from, long to) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_GET_LIST_MOST_COMMENTED)) {
            statement.setLong(1, to);
            statement.setLong(2, from);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                News news;
                while (resultSet.next()) {
                    news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setCreationDate(resultSet.getTimestamp(4));
                    news.setModificationDate(resultSet.getDate(5));
                    newsList.add(news);
                }
                return newsList;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public long getNumber() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_NEWS_GET_NUMBER)) {
            long result = 0;
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            }
            return result;
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> search(SearchCriteriaTO searchCriteria, long from, long to) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = SearchCriteriaHelper.generateQuery(searchCriteria);
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, to);
            statement.setLong(2, from);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                News news;
                while (resultSet.next()) {
                    news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setCreationDate(resultSet.getTimestamp(4));
                    news.setModificationDate(resultSet.getDate(5));
                    newsList.add(news);
                }
                return newsList;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void addTagsBinding(Long newsId, List<Long> tagIds) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_ADD_TAG_BINDING)) {
            for (Long tagId : tagIds) {
                statement.setLong(1, newsId);
                statement.setLong(2, tagId);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void addAuthorBinding(Long newsId, Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_ADD_AUTHOR_BINDING)) {
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void removeTagsBinding(Long newsId, List<Long> tagIds) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_REMOVE_TAG_BINDING)) {
            for (Long tagId : tagIds) {
                statement.setLong(1, newsId);
                statement.setLong(2, tagId);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void removeTagBinding(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_REMOVE_TAG_BINDING_BY_NEWS)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void removeAuthorBinding(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                     SQL_NEWS_REMOVE_AUTHOR_BINDING)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
