package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.*;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */
@Named
public class NewsTOServiceImpl implements NewsTOService {
    @Inject
    private NewsService newsService;

    @Inject
    private CommentService commentService;

    @Inject
    private AuthorService authorService;

    @Inject
    private TagService tagService;

    @Transactional(rollbackFor = { Exception.class })
    public void create(NewsTO newsTO) throws ServiceException {
        News news = newsTO.getNews();
        Author author = newsTO.getAuthor();
        List<Tag> tags = newsTO.getTags();
        Long newsId = newsService.create(news);
        newsService.addAuthorBinding(newsId, author.getAuthorId());
        List<Long> tagIds = new ArrayList<>();
        for (Tag tag : tags) {
            tagIds.add(tag.getTagId());
        }
        newsService.addTagsBinding(newsId, tagIds);
    }

    public NewsTO read(Long newsId) throws ServiceException {
        NewsTO newsTO = new NewsTO();
        News news = newsService.read(newsId);
        Author author = authorService.readByNews(newsId);
        List<Tag> tags = tagService.readListByNews(newsId);
        List<Comment> comments = commentService.readListByNews(newsId);
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);
        newsTO.setComments(comments);
        return newsTO;
    }

    @Transactional(rollbackFor = { Exception.class })
    public void delete(Long newsId) throws ServiceException {
        newsService.removeAuthorBinding(newsId);
        newsService.removeTagBinding(newsId);
        commentService.deleteByNews(newsId);
        newsService.delete(newsId);
    }

    public List<NewsTO> readList(int page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.readList(page);
        for (News news : newsList) {
            NewsTO newsTO = new NewsTO();
            List<Tag> tags = tagService.readListByNews(news.getNewsId());
            Author author = authorService.readByNews(news.getNewsId());
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTags(tags);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    public List<NewsTO> readListOrderByCommentsNumber(int page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.readListOrderByCommentsNumber(page);
        for (News news : newsList) {
            NewsTO newsTO = new NewsTO();
            List<Tag> tags = tagService.readListByNews(news.getNewsId());
            Author author = authorService.readByNews(news.getNewsId());
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTags(tags);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    public List<NewsTO> search(SearchCriteriaTO searchCriteria, int page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.search(searchCriteria, page);
        for (News news : newsList) {
            NewsTO newsTO = new NewsTO();
            List<Tag> tags = tagService.readListByNews(news.getNewsId());
            Author author = authorService.readByNews(news.getNewsId());
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTags(tags);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }
}
