package by.epam.task1.service;

import by.epam.task1.domain.Author;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */

/**
 * This interface provides methods for working with DAO layer
 * for instance <code>Author</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Author
 * @see by.epam.task1.service.GenericCRUDService
 */
public interface AuthorService extends GenericCRUDService<Author> {
    /**
     * Sets author's expire field to current time.
     *
     * @param authorId id of author which expired field is to be set
     * @throws ServiceException if DaoException is thrown
     */
    void makeExpired(Long authorId) throws ServiceException;

    /**
     * Returns a list of all authors.
     *
     * @return a list of all authors
     * @throws ServiceException if DaoException is thrown
     */
    List<Author> readListAll() throws ServiceException;

    /**
     * Returns a list of authors for page <code>page</code>.
     *
     * @param page number of page for which authors are to be returned
     * @return a list of authors
     * @throws ServiceException if DaoException is thrown
     */
    List<Author> readList(int page) throws ServiceException;

    /**
     * Returns an author by news with id <code>newsId</code>.
     *
     * @param newsId id of news which is to be returned
     * @return an author by news with id <code>newsId</code>
     * @throws ServiceException if DaoException is thrown
     */
    Author readByNews(Long newsId) throws ServiceException;
}
