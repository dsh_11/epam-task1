package by.epam.task1.service.impl;

import by.epam.task1.dao.CommentDao;
import by.epam.task1.exception.DaoException;
import by.epam.task1.domain.Comment;
import by.epam.task1.service.CommentService;
import by.epam.task1.exception.ServiceException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 15/02/2016.
 */
@Named
public class CommentServiceImpl implements CommentService {
    @Inject
    private CommentDao commentDao;

    public Long create(Comment comment) throws ServiceException {
        try {
            return commentDao.create(comment);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public Comment read(Long commentId) throws ServiceException {
        try {
            Comment comment = commentDao.read(commentId);
            if (comment == null) {
                throw new ServiceException("Comment with id " + commentId + " was not found.");
            }
            return comment;
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void update(Comment comment) throws ServiceException {
        try {
            commentDao.update(comment);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void delete(Long commentId) throws ServiceException {
        try {
            commentDao.delete(commentId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Comment> readListByNews(Long newsId) throws ServiceException {
        try {
            return commentDao.readListByNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void deleteByNews(Long newsId) throws ServiceException {
        try {
            commentDao.deleteByNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }
}