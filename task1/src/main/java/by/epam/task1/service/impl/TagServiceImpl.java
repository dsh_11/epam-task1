package by.epam.task1.service.impl;

import by.epam.task1.exception.DaoException;
import by.epam.task1.dao.TagDao;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.TagService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 15/02/2016.
 */
@Named
public class TagServiceImpl implements TagService {
    @Inject
    private TagDao tagDao;

    private static final int TAG_NUMBER_PER_PAGE = 15;

    public Long create(Tag tag) throws ServiceException {
        try {
            return tagDao.create(tag);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public Tag read(Long tagId) throws ServiceException {
        try {
            Tag tag = tagDao.read(tagId);
            if (tag == null) {
                throw new ServiceException("Tag with id " + tagId + " was not found.");
            }
            return tag;
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void update(Tag tag) throws ServiceException {
        try {
            tagDao.update(tag);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void delete(Long tagId) throws ServiceException {
        try {
            tagDao.removeNewsBinding(tagId);
            tagDao.delete(tagId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Tag> readListAll() throws ServiceException {
        try {
            return tagDao.readListAll();
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Tag> readList(int page) throws ServiceException {
        long from = (page - 1) * TAG_NUMBER_PER_PAGE + 1;
        long to = from + TAG_NUMBER_PER_PAGE;
        try {
            return tagDao.readList(from, to);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Tag> readListByNews(Long newsId) throws ServiceException {
        try {
            return tagDao.readListByNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }
}
