package by.epam.task1.service;

import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */

/**
 * This interface provides methods for working with DAO layer
 * for instance <code>Tag</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Tag
 * @see by.epam.task1.service.GenericCRUDService
 */
public interface TagService extends GenericCRUDService<Tag> {
    /**
     * Returns a list of all tags.
     *
     * @return a list of all tags
     * @throws ServiceException if DaoException is thrown
     */
    List<Tag> readListAll() throws ServiceException;

    /**
     * Returns a list of tags for page <code>page</code>.
     *
     * @param page number of page for which tags are to be returned
     * @return a list of tags
     * @throws ServiceException if DaoException is thrown
     */
    List<Tag> readList(int page) throws ServiceException;

    /**
     * Returns a list of tags by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which tags are to be returned
     * @return a list of tags
     * @throws ServiceException if DaoException is thrown
     */
    List<Tag> readListByNews(Long newsId) throws ServiceException;
}
