package by.epam.task1.service.impl;

import by.epam.task1.dao.AuthorDao;
import by.epam.task1.exception.DaoException;
import by.epam.task1.domain.Author;
import by.epam.task1.service.AuthorService;
import by.epam.task1.exception.ServiceException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */
@Named
public class AuthorServiceImpl implements AuthorService {
    @Inject
    private AuthorDao authorDao;

    private static final int AUTHOR_NUMBER_PER_PAGE = 10;

    public Long create(Author author) throws ServiceException {
        try {
            return authorDao.create(author);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public Author read(Long authorId) throws ServiceException {
        try {
            Author author = authorDao.read(authorId);
            if (author == null) {
                throw new ServiceException("Author with id " + authorId + " was not found.");
            }
            return author;
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void update(Author author) throws ServiceException {
        try {
            authorDao.update(author);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void delete(Long authorId) throws ServiceException {
        try {
            authorDao.delete(authorId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void makeExpired(Long authorId) throws ServiceException {
        try {
            Author author = new Author();
            author.setAuthorId(authorId);
            author.setExpired(new Date());
            authorDao.makeExpired(author);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Author> readListAll() throws ServiceException {
        try {
            return authorDao.readListAll();
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Author> readList(int page) throws ServiceException {
        long from = (page - 1) * AUTHOR_NUMBER_PER_PAGE + 1;
        long to = from + AUTHOR_NUMBER_PER_PAGE;
        try {
            return authorDao.readList(from, to);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public Author readByNews(Long newsId) throws ServiceException {
        try {
            Author author = authorDao.readByNews(newsId);
            if (author == null) {
                throw new ServiceException("Author by news with id " + newsId + " was not found.");
            }
            return author;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}
