package by.epam.task1.service.impl;

import by.epam.task1.exception.DaoException;
import by.epam.task1.dao.NewsDao;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.service.NewsService;
import by.epam.task1.exception.ServiceException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 15/02/2016.
 */
@Named
public class NewsServiceImpl implements NewsService {
    @Inject
    private NewsDao newsDao;

    private static final int NEWS_NUMBER_PER_PAGE = 15;

    public Long create(News news) throws ServiceException {
        try {
            return newsDao.create(news);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public News read(Long newsId) throws ServiceException {
        try {
            News news = newsDao.read(newsId);
            if (news == null) {
                throw new ServiceException("News with id " + newsId + " was not found.");
            }
            return news;
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void update(News news) throws ServiceException {
        try {
            newsDao.update(news);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void delete(Long newsId) throws ServiceException {
        try {
            newsDao.delete(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<News> readList(int page) throws ServiceException {
        long from = (page - 1) * NEWS_NUMBER_PER_PAGE + 1;
        long to = from + NEWS_NUMBER_PER_PAGE;
        try {
            return newsDao.readList(from, to);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<News> readListOrderByCommentsNumber(int page) throws ServiceException {
        long from = (page - 1) * NEWS_NUMBER_PER_PAGE + 1;
        long to = from + NEWS_NUMBER_PER_PAGE;
        try {
            return newsDao.readListOrderByCommentsNumber(from, to);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<News> search(SearchCriteriaTO searchCriteria, int page) throws ServiceException {
        long from = (page - 1) * NEWS_NUMBER_PER_PAGE + 1;
        long to = from + NEWS_NUMBER_PER_PAGE;
        try {
            return newsDao.search(searchCriteria, from, to);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public long getNumber() throws ServiceException {
        try {
            return newsDao.getNumber();
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void addAuthorBinding(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDao.addAuthorBinding(newsId, authorId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void addTagsBinding(Long newsId, List<Long> tagIds) throws ServiceException {
        try {
            newsDao.addTagsBinding(newsId, tagIds);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void removeAuthorBinding(Long newsId) throws ServiceException {
        try {
            newsDao.removeAuthorBinding(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void removeTagBinding(Long newsId) throws ServiceException {
        try {
            newsDao.removeTagBinding(newsId);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public void removeTagsBinding(Long newsId, List<Long> tagIds) throws ServiceException {
        try {
            newsDao.removeTagsBinding(newsId, tagIds);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }
}
