package by.epam.task1.service;

import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 15/02/2016.
 */

/**
 * This interface provides methods for working with DAO layer
 * for instance <code>News</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.News
 * @see by.epam.task1.service.GenericCRUDService
 */
public interface NewsService extends GenericCRUDService<News> {
    /**
     * Returns a list of news for page <code>page</code>.
     *
     * @param page number of page for which news are to be returned
     * @return a list of news
     * @throws ServiceException if DaoException is thrown
     */
    List<News> readList(int page) throws ServiceException;

    /**
     * Returns a list of news for page <code>page</code>
     * ordered by the number of comments.
     *
     * @param page number of page for which news are to be returned
     * @return a list of news
     * @throws ServiceException if DaoException is thrown
     */
    List<News> readListOrderByCommentsNumber(int page) throws ServiceException;

    /**
     * Returns a list of news satisfying <code>searchCriteria</code> for
     * page <code>page</code>.
     *
     * @param searchCriteria search criteria by which news are to be searched
     * @param page number of page for which news are to be returned
     * @return a list of news
     * @throws ServiceException if DaoException is thrown
     */
    List<News> search(SearchCriteriaTO searchCriteria, int page) throws ServiceException;

    /**
     * Returns number of all news.
     *
     * @return number of all news
     * @throws ServiceException if DaoException is thrown
     */
    long getNumber() throws ServiceException;

    /**
     * Binds news with id <code>newsId</code> with author
     * with id <code>authorId</code>.
     *
     * @param newsId id of news which should be bound with author
     * @param authorId id of author which should be bound with news
     * @throws ServiceException if DaoException is thrown
     */
    void addAuthorBinding(Long newsId, Long authorId) throws ServiceException;

    /**
     * Binds news with id <code>newsId</code> with tags list <code>tags</code>.
     *
     * @param newsId id of news which should be bound with tag
     * @param tagIds ids of tags which should be bound with news
     * @throws ServiceException if DaoException is thrown
     */
    void addTagsBinding(Long newsId, List<Long> tagIds) throws ServiceException;

    /**
     * Removes binding between news with id <code>newsId</code> and author.
     *
     * @param newsId id of news which should be unbound from author
     * @throws ServiceException if DaoException is thrown
     */
    void removeAuthorBinding(Long newsId) throws ServiceException;

    /**
     * Removes binding between news with id <code>newsId</code> and all tags.
     *
     * @param newsId id of news which should be unbound from tags
     * @throws ServiceException if DaoException is thrown
     */
    void removeTagBinding(Long newsId) throws ServiceException;

    /**
     * Removes binding between news with id <code>newsId</code> and tags
     * list <code>tags</code>.
     *
     * @param newsId id of news which should be unbound from tags
     * @param tagIds ids of tags which should be unbound from news
     * @throws ServiceException if DaoException is thrown
     */
    void removeTagsBinding(Long newsId, List<Long> tagIds) throws ServiceException;
}
