package by.epam.task1.util;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 19/02/2016.
 */
public class SearchCriteriaHelper {
    private static final String searchQueryStart = "SELECT * FROM (SELECT a.*, ROWNUM rn FROM " +
            "(SELECT DISTINCT news.news_id, title, short_text, creation_date, modification_date " +
            "FROM news ";
    private static final String searchQueryJoinAuthor = "INNER JOIN news_authors ON news.news_id = " +
            "news_authors.news_id ";
    private static final String searchQueryJoinTags = "INNER JOIN news_tags ON news.news_id = " +
            "news_tags.news_id ";
    private static final String searchQueryEnd = " ORDER BY creation_date DESC) a WHERE ROWNUM < ?) " +
            "WHERE rn >= ?";

    public static String generateQuery(SearchCriteriaTO searchCriteria) {
        Long authorId = searchCriteria.getAuthorId();
        List<Long> tagIds = searchCriteria.getTagIds();
        StringBuilder query = new StringBuilder(searchQueryStart);
        if ((authorId == null) && (tagIds == null || tagIds.isEmpty())) {
            query.append(searchQueryEnd);
            return query.toString();
        } else if (!(authorId == null) && !(tagIds == null || tagIds.isEmpty())) {
            query.append(searchQueryJoinAuthor);
            query.append(searchQueryJoinTags);
            query.append("WHERE ");
            generateAuthorQuery(query, authorId);
            query.append(" AND (");
            generateTagsQuery(query, tagIds);
            query.append(")");
        } else if (authorId != null) {
            query.append(searchQueryJoinAuthor);
            query.append("WHERE ");
            generateAuthorQuery(query, authorId);
        } else {
            query.append(searchQueryJoinTags);
            query.append("WHERE ");
            generateTagsQuery(query, tagIds);
        }
        query.append(searchQueryEnd);
        return query.toString();
    }

    private static void generateAuthorQuery(StringBuilder query, Long authorId) {
        query.append("author_id = ");
        query.append(authorId);
    }

    private static void generateTagsQuery(StringBuilder query, List<Long> tagIds) {
        long tagsNumber = tagIds.size();
        long i = 1;
        for (Long tagId : tagIds) {
            query.append("tag_id = ");
            query.append(tagId);
            if (i != tagsNumber) {
                query.append(" OR ");
            }
            i++;
        }
    }
}
