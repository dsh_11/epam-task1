package by.epam.task1.domain;

import java.util.Date;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class News {
    private Long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Date creationDate;
    private Date modificationDate;

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        News obj = (News) o;

        return ((newsId == obj.getNewsId()) ||
                        (newsId != null && newsId.equals(obj.getNewsId()))) &&
                ((title == obj.getTitle()) ||
                        (title != null && title.equals(obj.getTitle()))) &&
                ((shortText == obj.getShortText()) ||
                        (shortText != null && shortText.equals(obj.getShortText()))) &&
                ((fullText == obj.getFullText()) ||
                        (fullText != null && fullText.equals(obj.getFullText()))) &&
                ((creationDate == obj.getCreationDate()) ||
                        (creationDate != null && creationDate.equals(obj.getCreationDate()))) &&
                ((modificationDate == obj.getModificationDate()) ||
                        (modificationDate != null && modificationDate.equals(obj.getModificationDate())));
    }

    public int hashCode() {
        int result = newsId != null ? newsId.hashCode() : 0;
        result = 7 * result + (title != null ? title.hashCode() : 0);
        result = 7 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 7 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 7 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 7 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }
}
