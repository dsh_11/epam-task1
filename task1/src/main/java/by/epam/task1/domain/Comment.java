package by.epam.task1.domain;

import java.util.Date;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class Comment {
    private Long commentId;
    private Long newsId;
    private String commentText;
    private Date creationDate;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        Comment obj = (Comment) o;

        return ((commentId == obj.commentId) ||
                        (commentId != null && commentId.equals(obj.getCommentId()))) &&
                ((newsId == obj.getNewsId()) ||
                        (newsId != null && newsId.equals(obj.getNewsId()))) &&
                ((commentText == obj.getCommentText()) ||
                        (commentText != null && commentText.equals(obj.getCommentText()))) &&
                ((creationDate == obj.getCreationDate()) ||
                        (creationDate != null && creationDate.equals(obj.getCreationDate())));
    }

    public int hashCode() {
        int result = commentId != null ? commentId.hashCode() : 0;
        result = 11 * result + (newsId != null ? newsId.hashCode() : 0);
        result = 11 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 11 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }
}
