package by.epam.task1.domain;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class Tag {
    private Long tagId;
    private String tagName;

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        Tag obj = (Tag) o;

        return ((tagId == obj.getTagId()) ||
                        (tagId != null && tagId.equals(obj.getTagId()))) &&
                ((tagName == obj.getTagName()) ||
                        (tagName != null && tagName.equals(obj.getTagName())));
    }

    public int hashCode() {
        int result = tagId != null ? tagId.hashCode() : 0;
        result = 7 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }
}
