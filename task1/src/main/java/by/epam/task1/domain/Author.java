package by.epam.task1.domain;

import java.util.Date;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class Author {
    private Long authorId;
    private String authorName;
    private Date expired;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        Author obj = (Author) o;

        return ((authorId == obj.getAuthorId()) ||
                        (authorId != null && authorId.equals(obj.getAuthorId()))) &&
                ((authorName == obj.getAuthorName()) ||
                        (authorName != null && authorName.equals(obj.getAuthorName()))) &&
                ((expired == obj.getExpired()) ||
                        (expired != null && expired.equals(obj.getExpired())));
    }

    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 11 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 11 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }
}
