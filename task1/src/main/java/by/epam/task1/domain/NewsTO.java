package by.epam.task1.domain;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class NewsTO {
    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
