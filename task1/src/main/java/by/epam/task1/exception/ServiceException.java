package by.epam.task1.exception;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */
public class ServiceException extends Exception {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
