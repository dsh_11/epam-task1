package by.epam.task1.service.impl;

import by.epam.task1.dao.NewsDao;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.service.NewsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Darya_Yeuzhyk on 18/02/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class NewsServiceImplTest {
    @Mock
    private NewsDao newsDao;

    @InjectMocks
    private NewsService newsService = new NewsServiceImpl();

    @Test
    public void testCreate() throws Exception {
        Long expectedNewsId = 1L;
        News news = new News();
        when(newsDao.create(news)).thenReturn(expectedNewsId);
        Long actualNewsId = newsService.create(news);

        verify(newsDao, times(1)).create(news);
        assertEquals(expectedNewsId, actualNewsId);
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;
        News expectedNews = new News();
        when(newsDao.read(newsId)).thenReturn(expectedNews);
        News actualNews = newsService.read(newsId);

        verify(newsDao, times(1)).read(newsId);
        assertEquals(expectedNews, actualNews);
    }

    @Test
    public void testUpdate() throws Exception {
        News news = new News();
        newsService.update(news);

        verify(newsDao, times(1)).update(news);
    }

    @Test
    public void testDelete() throws Exception {
        Long newsId = 1L;
        newsService.delete(newsId);

        verify(newsDao, times(1)).delete(newsId);
    }

    @Test
    public void testReadList() throws Exception {
        int newsNumberPerPage = 15;
        int page = 1;
        long from = (page - 1) * newsNumberPerPage + 1;
        long to = from + newsNumberPerPage;
        when(newsDao.readList(from, to)).thenReturn(new ArrayList<>());
        List<News> newsList = newsService.readList(page);

        verify(newsDao, times(1)).readList(from, to);
        assertNotNull(newsList);
    }

    @Test
    public void testReadListOrderByCommentsNumber() throws Exception {
        int newsNumberPerPage = 15;
        int page = 1;
        long from = (page - 1) * newsNumberPerPage + 1;
        long to = from + newsNumberPerPage;
        when(newsDao.readListOrderByCommentsNumber(from, to)).thenReturn(new ArrayList<>());
        List<News> newsList = newsService.readListOrderByCommentsNumber(page);

        verify(newsDao, times(1)).readListOrderByCommentsNumber(from, to);
        assertNotNull(newsList);
    }

    @Test
    public void testSearch() throws Exception {
        int newsNumberPerPage = 15;
        int page = 1;
        long from = (page - 1) * newsNumberPerPage + 1;
        long to = from + newsNumberPerPage;
        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        when(newsDao.search(searchCriteria, from, to)).thenReturn(new ArrayList<>());
        List<News> newsList = newsService.search(searchCriteria, page);

        verify(newsDao, times(1)).search(searchCriteria, from, to);
        assertNotNull(newsList);
    }

    @Test
    public void testGetNumber() throws Exception {
        long expectedNumber = 3;
        when(newsDao.getNumber()).thenReturn(expectedNumber);
        long actualNumber = newsService.getNumber();

        verify(newsDao, times(1)).getNumber();
        assertEquals(expectedNumber, actualNumber);
    }

    @Test
    public void testAddAuthorBinding() throws Exception {
        Long newsId = 1L;
        Long authorId = 1L;
        newsService.addAuthorBinding(newsId, authorId);

        verify(newsDao, times(1)).addAuthorBinding(newsId, authorId);
    }

    @Test
    public void testAddTagsBinding() throws Exception {
        Long newsId = 1L;
        List<Long> tagIds = new ArrayList<>();
        newsService.addTagsBinding(newsId, tagIds);

        verify(newsDao, times(1)).addTagsBinding(newsId, tagIds);
    }

    @Test
    public void testRemoveAuthorBinding() throws Exception {
        Long newsId = 1L;
        newsService.removeAuthorBinding(newsId);

        verify(newsDao, times(1)).removeAuthorBinding(newsId);
    }

    @Test
    public void testRemoveTagBinding() throws Exception {
        Long newsId = 1L;
        newsService.removeTagBinding(newsId);

        verify(newsDao, times(1)).removeTagBinding(newsId);
    }

    @Test
    public void testRemoveTagsBinding() throws Exception {
        Long newsId = 1L;
        List<Long> tagIds = new ArrayList<>();
        newsService.removeTagsBinding(newsId, tagIds);

        verify(newsDao, times(1)).removeTagsBinding(newsId, tagIds);
    }
}
