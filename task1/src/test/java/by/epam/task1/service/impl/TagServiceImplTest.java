package by.epam.task1.service.impl;

import by.epam.task1.dao.TagDao;
import by.epam.task1.domain.Tag;
import by.epam.task1.service.TagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Darya_Yeuzhyk on 18/02/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class TagServiceImplTest {
    @Mock
    private TagDao tagDao;

    @InjectMocks
    private TagService tagService = new TagServiceImpl();

    @Test
    public void testCreate() throws Exception {
        Long expectedTagId = 1L;
        Tag tag = new Tag();
        when(tagDao.create(tag)).thenReturn(expectedTagId);
        Long actualTagId = tagService.create(tag);

        verify(tagDao, times(1)).create(tag);
        assertEquals(expectedTagId, actualTagId);
    }

    @Test
    public void testRead() throws Exception {
        Long tagId = 1L;
        Tag expectedTag = new Tag();
        when(tagDao.read(tagId)).thenReturn(expectedTag);
        Tag actualTag = tagService.read(tagId);

        verify(tagDao, times(1)).read(tagId);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testUpdate() throws Exception {
        Tag tag = new Tag();
        tagService.update(tag);

        verify(tagDao, times(1)).update(tag);
    }

    @Test
    public void testDelete() throws Exception {
        Long tagId = 1L;
        tagService.delete(tagId);

        verify(tagDao, times(1)).delete(tagId);
    }

    @Test
    public void testReadListAll() throws Exception {
        when(tagDao.readListAll()).thenReturn(new ArrayList<>());
        List<Tag> tags = tagService.readListAll();

        verify(tagDao, times(1)).readListAll();
        assertNotNull(tags);
    }

    @Test
    public void testReadList() throws Exception {
        int tagNumberPerPage = 15;
        int page = 1;
        long from = (page - 1) * tagNumberPerPage + 1;
        long to = from + tagNumberPerPage;
        when(tagDao.readList(from, to)).thenReturn(new ArrayList<>());
        List<Tag> tags = tagService.readList(page);

        verify(tagDao, times(1)).readList(from, to);
        assertNotNull(tags);
    }

    @Test
    public void testReadListByNews() throws Exception {
        Long newsId = 1L;
        when(tagDao.readListByNews(newsId)).thenReturn(new ArrayList<>());
        List<Tag> tags = tagService.readListByNews(newsId);

        verify(tagDao, times(1)).readListByNews(newsId);
        assertNotNull(tags);
    }
}
