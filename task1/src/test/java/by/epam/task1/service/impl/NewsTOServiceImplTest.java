package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Darya_Yeuzhyk on 18/02/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class NewsTOServiceImplTest {
    @Mock
    private AuthorService authorService;

    @Mock
    private CommentService commentService;

    @Mock
    private NewsService newsService;

    @Mock
    private TagService tagService;

    @InjectMocks
    private NewsTOService newsTOService = new NewsTOServiceImpl();

    @Test
    public void testCreate() throws Exception {
        NewsTO newsTO = new NewsTO();
        News news = new News();
        Author author = new Author();
        List<Tag> tags = new ArrayList<>();
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);
        Long newsId = 1L;
        when(newsService.create(news)).thenReturn(newsId);
        newsTOService.create(newsTO);

        List<Long> tagIds = new ArrayList<>();
        for (Tag tag : tags) {
            tagIds.add(tag.getTagId());
        }

        verify(newsService, times(1)).create(news);
        verify(newsService, times(1)).addAuthorBinding(newsId, author.getAuthorId());
        verify(newsService, times(1)).addTagsBinding(newsId, tagIds);
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;
        News news = new News();
        Author author = new Author();
        List<Tag> tags = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        when(newsService.read(newsId)).thenReturn(news);
        when(authorService.readByNews(newsId)).thenReturn(author);
        when(tagService.readListByNews(newsId)).thenReturn(tags);
        when(commentService.readListByNews(newsId)).thenReturn(comments);
        NewsTO newsTO = newsTOService.read(newsId);

        verify(newsService, times(1)).read(newsId);
        verify(authorService, times(1)).readByNews(newsId);
        verify(tagService, times(1)).readListByNews(newsId);
        verify(commentService, times(1)).readListByNews(newsId);
        assertEquals(news, newsTO.getNews());
        assertEquals(author, newsTO.getAuthor());
        assertEquals(tags, newsTO.getTags());
        assertEquals(comments, newsTO.getComments());
    }

    @Test
    public void testDelete() throws Exception {
        Long newsId = 1L;
        newsTOService.delete(newsId);

        verify(newsService, times(1)).removeAuthorBinding(newsId);
        verify(newsService, times(1)).removeTagBinding(newsId);
        verify(commentService, times(1)).deleteByNews(newsId);
        verify(newsService, times(1)).delete(newsId);
    }

    @Test
    public void testReadList() throws Exception {
        int page = 1;
        List<News> newsList = new ArrayList<>();
        News news1 = new News();
        News news2 = new News();
        Long newsId1 = 1L;
        Long newsId2 = 2L;
        news1.setNewsId(newsId1);
        news2.setNewsId(newsId2);
        newsList.add(news1);
        newsList.add(news2);
        when(newsService.readList(page)).thenReturn(newsList);
        for (News news : newsList) {
            Long newsId = news.getNewsId();
            when(tagService.readListByNews(newsId)).thenReturn(new ArrayList<>());
            when(authorService.readByNews(newsId)).thenReturn(new Author());
        }
        List<NewsTO> newsTOList = newsTOService.readList(page);

        verify(newsService, times(1)).readList(page);
        verify(tagService, times(1)).readListByNews(newsId1);
        verify(authorService, times(1)).readByNews(newsId1);
        verify(tagService, times(1)).readListByNews(newsId2);
        verify(authorService, times(1)).readByNews(newsId2);
        assertFalse(newsTOList.isEmpty());
    }

    @Test
    public void testReadListOrderByCommentsNumber() throws Exception {
        int page = 1;
        List<News> newsList = new ArrayList<>();
        News news1 = new News();
        News news2 = new News();
        Long newsId1 = 1L;
        Long newsId2 = 2L;
        news1.setNewsId(newsId1);
        news2.setNewsId(newsId2);
        newsList.add(news1);
        newsList.add(news2);
        when(newsService.readListOrderByCommentsNumber(page)).thenReturn(newsList);
        for (News news : newsList) {
            Long newsId = news.getNewsId();
            when(tagService.readListByNews(newsId)).thenReturn(new ArrayList<>());
            when(authorService.readByNews(newsId)).thenReturn(new Author());
        }
        List<NewsTO> newsTOList = newsTOService.readListOrderByCommentsNumber(page);

        verify(newsService, times(1)).readListOrderByCommentsNumber(page);
        verify(tagService, times(1)).readListByNews(newsId1);
        verify(authorService, times(1)).readByNews(newsId1);
        verify(tagService, times(1)).readListByNews(newsId2);
        verify(authorService, times(1)).readByNews(newsId2);
        assertFalse(newsTOList.isEmpty());
    }

    @Test
    public void testSearch() throws Exception {
        int page = 1;
        List<News> newsList = new ArrayList<>();
        News news1 = new News();
        News news2 = new News();
        Long newsId1 = 1L;
        Long newsId2 = 2L;
        news1.setNewsId(newsId1);
        news2.setNewsId(newsId2);
        newsList.add(news1);
        newsList.add(news2);
        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        when(newsService.search(searchCriteria, page)).thenReturn(newsList);
        for (News news : newsList) {
            Long newsId = news.getNewsId();
            when(tagService.readListByNews(newsId)).thenReturn(new ArrayList<>());
            when(authorService.readByNews(newsId)).thenReturn(new Author());
        }
        List<NewsTO> newsTOList = newsTOService.search(searchCriteria, page);

        verify(newsService, times(1)).search(searchCriteria, page);
        verify(tagService, times(1)).readListByNews(newsId1);
        verify(authorService, times(1)).readByNews(newsId1);
        verify(tagService, times(1)).readListByNews(newsId2);
        verify(authorService, times(1)).readByNews(newsId2);
        assertFalse(newsTOList.isEmpty());
    }
}
