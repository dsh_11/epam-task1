package by.epam.task1.service.impl;

import by.epam.task1.dao.AuthorDao;
import by.epam.task1.domain.Author;
import by.epam.task1.service.AuthorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Darya_Yeuzhyk on 18/02/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class AuthorServiceImplTest {
    @Mock
    private AuthorDao authorDao;

    @InjectMocks
    private AuthorService authorService = new AuthorServiceImpl();

    @Test
    public void testCreate() throws Exception {
        Long expectedAuthorId = 1L;
        Author author = new Author();
        when(authorDao.create(author)).thenReturn(expectedAuthorId);
        Long actualAuthorId = authorService.create(author);

        verify(authorDao, times(1)).create(author);
        assertEquals(expectedAuthorId, actualAuthorId);
    }

    @Test
    public void testRead() throws Exception {
        Long authorId = 1L;
        Author expectedAuthor = new Author();
        when(authorDao.read(authorId)).thenReturn(expectedAuthor);
        Author actualAuthor = authorService.read(authorId);

        verify(authorDao, times(1)).read(authorId);
        assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void testUpdate() throws Exception {
        Author author = new Author();
        authorService.update(author);

        verify(authorDao, times(1)).update(author);
    }

    @Test
    public void testDelete() throws Exception {
        Long authorId = 1L;
        authorService.delete(authorId);

        verify(authorDao, times(1)).delete(authorId);
    }


    @Test
    public void testMakeExpired() throws Exception {
        Long authorId = 1L;
        authorService.makeExpired(authorId);

        verify(authorDao, times(1)).makeExpired(any(Author.class));
    }

    @Test
    public void testReadListAll() throws Exception {
        when(authorDao.readListAll()).thenReturn(new ArrayList<>());
        List<Author> authors = authorService.readListAll();

        verify(authorDao, times(1)).readListAll();
        assertNotNull(authors);
    }

    @Test
    public void testReadList() throws Exception {
        int authorNumberPerPage = 10;
        int page = 1;
        long from = (page - 1) * authorNumberPerPage + 1;
        long to = from + authorNumberPerPage;
        when(authorDao.readList(from, to)).thenReturn(new ArrayList<>());
        List<Author> authors = authorService.readList(page);

        verify(authorDao, times(1)).readList(from, to);
        assertNotNull(authors);
    }

    @Test
    public void testReadByNews() throws Exception {
        Long newsId = 1L;
        when(authorDao.readByNews(newsId)).thenReturn(new Author());
        Author author = authorService.readByNews(newsId);

        verify(authorDao, times(1)).readByNews(newsId);
        assertNotNull(author);
    }
}
