package by.epam.task1.service.impl;

import by.epam.task1.dao.CommentDao;
import by.epam.task1.domain.Comment;
import by.epam.task1.service.CommentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Darya_Yeuzhyk on 18/02/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDao;

    @InjectMocks
    private CommentService commentService = new CommentServiceImpl();

    @Test
    public void testCreate() throws Exception {
        Long expectedCommentId = 1L;
        Comment comment = new Comment();
        when(commentDao.create(comment)).thenReturn(expectedCommentId);
        Long actualCommentId = commentService.create(comment);

        verify(commentDao, times(1)).create(comment);
        assertEquals(expectedCommentId, actualCommentId);
    }

    @Test
    public void testRead() throws Exception {
        Long commentId = 1L;
        Comment expectedComment = new Comment();
        when(commentDao.read(commentId)).thenReturn(expectedComment);
        Comment actualComment = commentService.read(commentId);

        verify(commentDao, times(1)).read(commentId);
        assertEquals(expectedComment, actualComment);
    }

    @Test
    public void testUpdate() throws Exception {
        Comment comment = new Comment();
        commentService.update(comment);

        verify(commentDao, times(1)).update(comment);
    }

    @Test
    public void testDelete() throws Exception {
        Long commentId = 1L;
        commentService.delete(commentId);

        verify(commentDao, times(1)).delete(commentId);
    }

    @Test
    public void testReadListByNews() throws Exception {
        Long newsId = 1L;
        when(commentDao.readListByNews(newsId)).thenReturn(new ArrayList<>());
        List<Comment> comments = commentService.readListByNews(newsId);

        verify(commentDao, times(1)).readListByNews(newsId);
        assertNotNull(comments);
    }

    @Test
    public void testDeleteByNews() throws Exception {
        Long newsId = 1L;
        commentService.deleteByNews(newsId);

        verify(commentDao, times(1)).deleteByNews(newsId);
    }
}
