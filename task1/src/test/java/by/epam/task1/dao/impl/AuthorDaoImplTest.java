package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDao;
import by.epam.task1.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import static org.junit.Assert.*;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 15/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = {"/context-test.xml"})
@DatabaseSetup(value = "AuthorDaoImplTest.xml")
@DatabaseTearDown(value = "AuthorDaoImplTest.xml",
        type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {
    @Inject
    private AuthorDao authorDao;

    @Test
    public void testCreate() throws Exception {
        String authorName = "Mary Jane";
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorName(authorName);
        Long authorId = authorDao.create(expectedAuthor);

        Author actualAuthor = authorDao.read(authorId);
        assertEquals(authorName, actualAuthor.getAuthorName());
    }

    @Test
    public void testRead() throws Exception {
        Long authorId = 1L;
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorId(authorId);
        expectedAuthor.setAuthorName("John Smith");
        Author actualAuthor = authorDao.read(authorId);

        assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void testUpdate() throws Exception {
        Long authorId = 1L;
        String authorName = "Kalie White";
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorId(authorId);
        expectedAuthor.setAuthorName(authorName);
        authorDao.update(expectedAuthor);

        Author actualAuthor = authorDao.read(authorId);
        assertEquals(authorName, actualAuthor.getAuthorName());
    }

    @Test
    public void testDelete() throws Exception {
        Long authorId = 1L;
        authorDao.delete(authorId);
        Author author = authorDao.read(authorId);

        assertNull(author);
    }

    @Test
    public void testReadListAll() throws Exception {
        List<Author> authors = authorDao.readListAll();
        int expectedSize = 3;
        String authorName1 = "Jack Sheppard";
        String authorName2 = "John Smith";
        String authorName3 = "Mary Jane";

        assertEquals(expectedSize, authors.size());
        assertEquals(authorName1, authors.get(0).getAuthorName());
        assertEquals(authorName2, authors.get(1).getAuthorName());
        assertEquals(authorName3, authors.get(2).getAuthorName());
    }

    @Test
    public void testReadList() throws Exception {
        int from = 2;
        int to = 4;
        List<Author> authors = authorDao.readList(from, to);
        int expectedSize = 2;
        String authorName1 = "John Smith";
        String authorName2 = "Mary Jane";

        assertEquals(expectedSize, authors.size());
        assertEquals(authorName1, authors.get(0).getAuthorName());
        assertEquals(authorName2, authors.get(1).getAuthorName());
    }

    @Test
    public void testReadByNews() throws Exception {
        Long newsId = 1L;
        Long authorId = 3L;
        String authorName = "Jack Sheppard";
        Author author = authorDao.readByNews(newsId);

        assertEquals(authorId, author.getAuthorId());
        assertEquals(authorName, author.getAuthorName());
    }

    @Test
    public void testMakeExpired() throws Exception {
        Long authorId = 1L;
        Date expired = new Date();
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorId(authorId);
        expectedAuthor.setExpired(expired);
        authorDao.makeExpired(expectedAuthor);
        Author actualAuthor = authorDao.read(authorId);

        assertEquals(expired, actualAuthor.getExpired());
    }
}