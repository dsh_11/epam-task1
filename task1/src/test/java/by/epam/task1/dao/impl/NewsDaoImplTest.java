package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDao;
import by.epam.task1.dao.NewsDao;
import by.epam.task1.dao.TagDao;
import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import static org.junit.Assert.*;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Darya_Yeuzhyk on 17/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = {"/context-test.xml"})
@DatabaseSetup(value = "NewsDaoImplTest.xml")
@DatabaseTearDown(value = "NewsDaoImplTest.xml",
        type = DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {
    @Inject
    private NewsDao newsDao;

    @Inject
    private TagDao tagDao;

    @Inject
    private AuthorDao authorDao;

    @Test
    public void testCreate() throws Exception {
        Date creationDate = new Date();
        News expectedNews = new News();
        expectedNews.setTitle("title");
        expectedNews.setShortText("short text");
        expectedNews.setFullText("full text");
        expectedNews.setCreationDate(creationDate);
        expectedNews.setModificationDate(DateUtils.truncate(creationDate, Calendar.DATE));
        Long newsId = newsDao.create(expectedNews);
        expectedNews.setNewsId(newsId);
        News actualNews = newsDao.read(newsId);

        assertEquals(expectedNews, actualNews);
    }

    @Test
    public void testRead() throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date creationDate = format.parse("2016-01-20 16:10:10");
        News news = new News();
        news.setNewsId(1L);
        news.setTitle("title1");
        news.setShortText("short_text1");
        news.setFullText("full_text1");
        news.setCreationDate(creationDate);
        news.setModificationDate(DateUtils.truncate(creationDate, Calendar.DATE));
        News readNews = newsDao.read(1L);

        assertEquals(news, readNews);
    }

    @Test
    public void testUpdate() throws Exception {
        String title = "title12";
        Date modificationDate = DateUtils.truncate(new Date(), Calendar.DATE);
        Long newsId = 1L;
        News news = new News();
        news.setNewsId(newsId);
        news.setTitle(title);
        news.setShortText("short_text1");
        news.setFullText("full_text1");
        news.setModificationDate(modificationDate);
        newsDao.update(news);
        News updatedNews = newsDao.read(newsId);

        assertEquals(title, updatedNews.getTitle());
    }

    @Test
    public void testDelete() throws Exception {
        Long newsId = 3L;
        newsDao.delete(newsId);
        News news = newsDao.read(newsId);

        assertNull(news);
    }

    @Test
    public void testReadList() throws Exception {
        int from = 2;
        int to = 3;
        List<News> newsList = newsDao.readList(from, to);
        int expectedSize = 1;
        String newsTitle = "title2";

        assertEquals(expectedSize, newsList.size());
        assertEquals(newsTitle, newsList.get(0).getTitle());
    }

    @Test
    public void testReadListOrderByCommentsNumber() throws Exception {
        int from = 1;
        int to = 3;
        List<News> newsList = newsDao.readListOrderByCommentsNumber(from, to);
        int expectedSize = 2;
        String newsTitle1 = "title2";
        String newsTitle2 = "title1";

        assertEquals(expectedSize, newsList.size());
        assertEquals(newsTitle1, newsList.get(0).getTitle());
        assertEquals(newsTitle2, newsList.get(1).getTitle());
    }

    @Test
    public void testGetNumber() throws Exception {
        long expectedNumber = 5;
        long actualNumber = newsDao.getNumber();

        assertEquals(expectedNumber, actualNumber);
    }

    @Test
    public void testSearch() throws Exception {
        int from = 1;
        int to = 3;
        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        Long authorId = 3L;
        List<Long> tagIds = new ArrayList<>();
        Long tagId1 = 1L;
        Long tagId2 = 3L;
        tagIds.add(tagId1);
        tagIds.add(tagId2);
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIds(tagIds);
        List<News> newsList = newsDao.search(searchCriteria, from, to);
        int expectedSize = 2;
        Long expectedNewsId1 = 2L;
        Long expectedNewsId2 = 4L;

        assertEquals(expectedSize, newsList.size());
        assertEquals(expectedNewsId1, newsList.get(0).getNewsId());
        assertEquals(expectedNewsId2, newsList.get(1).getNewsId());
    }

    @Test
    public void testAddTagsBinding() throws Exception {
        Long newsId = 3L;
        Long tagId1 = 1L;
        Long tagId2 = 2L;
        List<Long> expectedTagIds = new ArrayList<>();
        expectedTagIds.add(tagId1);
        expectedTagIds.add(tagId2);
        List<Tag> actualTags = tagDao.readListByNews(newsId);

        assertTrue(actualTags.isEmpty());

        newsDao.addTagsBinding(newsId, expectedTagIds);
        actualTags = tagDao.readListByNews(newsId);

        List<Long> actualTagIds = new ArrayList<>();
        for (Tag tag : actualTags) {
            actualTagIds.add(tag.getTagId());
        }

        assertEquals(expectedTagIds, actualTagIds);
    }

    @Test
    public void testAddAuthorBinding() throws Exception {
        Long newsId = 1L;
        Long authorId = 1L;
        Author author = authorDao.readByNews(newsId);

        assertNull(author);

        newsDao.addAuthorBinding(newsId, authorId);
        author = authorDao.readByNews(newsId);

        assertEquals(authorId, author.getAuthorId());
    }

    @Test
    public void testRemoveTagsBinding() throws Exception {
        Long newsId = 1L;
        int expectedSize = 2;
        List<Tag> tags = tagDao.readListByNews(newsId);

        assertEquals(expectedSize, tags.size());

        List<Long> tagIds = new ArrayList<>();
        tagIds.add(3L);
        newsDao.removeTagsBinding(newsId, tagIds);
        tags = tagDao.readListByNews(newsId);
        expectedSize = 1;

        assertEquals(expectedSize, tags.size());
    }

    @Test
    public void testRemoveTagBinding() throws Exception {
        Long newsId = 1L;
        int expectedSize = 2;
        List<Tag> tags = tagDao.readListByNews(newsId);

        assertEquals(expectedSize, tags.size());

        newsDao.removeTagBinding(newsId);
        tags = tagDao.readListByNews(newsId);

        assertTrue(tags.isEmpty());
    }

    @Test
    public void testRemoveAuthorBinding() throws Exception {
        Long newsId = 2L;
        Author author = authorDao.readByNews(newsId);

        assertNotNull(author);

        newsDao.removeAuthorBinding(newsId);
        author = authorDao.readByNews(newsId);

        assertNull(author);
    }
}
