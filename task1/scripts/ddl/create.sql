CREATE TABLE users (
  user_id NUMBER(20) NOT NULL,
  user_name NVARCHAR2(50) NOT NULL,
  login VARCHAR2(30) NOT NULL,
  password VARCHAR2(30) NOT NULL,
  CONSTRAINT user_id_pk PRIMARY KEY(user_id)
);

CREATE TABLE roles (
  user_id NUMBER(20) NOT NULL,
  role_name VARCHAR2(50) NOT NULL,
  CONSTRAINT roles_users FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE news(
  news_id NUMBER(20) NOT NULL,
  title NVARCHAR2(30) NOT NULL,
  short_text NVARCHAR2(100) NOT NULL,
  full_text NVARCHAR2(2000) NOT NULL,
  creation_date TIMESTAMP NOT NULL,
  modification_date DATE NOT NULL,
  CONSTRAINT news_id_pk PRIMARY KEY(news_id)
);

CREATE TABLE authors(
  author_id NUMBER(20) NOT NULL,
  author_name NVARCHAR2(30) NOT NULL,
  expired TIMESTAMP,
  CONSTRAINT author_id_pk PRIMARY KEY(author_id)
);

CREATE TABLE news_authors(
  news_id NUMBER(20) NOT NULL,
  author_id NUMBER(20) NOT NULL,
  CONSTRAINT news_author_news FOREIGN KEY(news_id) REFERENCES news(news_id),
  CONSTRAINT news_author_author FOREIGN KEY(author_id) REFERENCES authors(author_id)
);

CREATE TABLE comments(
  comment_id NUMBER(20) NOT NULL,
  news_id NUMBER(20) NOT NULL,
  comment_text NVARCHAR2(100) NOT NULL,
  creation_date TIMESTAMP NOT NULL,
  CONSTRAINT comment_id_pk PRIMARY KEY(comment_id),
  CONSTRAINT comments_news FOREIGN KEY(news_id) REFERENCES news(news_id)
);

CREATE TABLE tags(
  tag_id NUMBER(20) NOT NULL,
  tag_name NVARCHAR2(30) NOT NULL,
  CONSTRAINT tag_id_pk PRIMARY KEY(tag_id)
);

CREATE TABLE news_tags(
  news_id NUMBER(20) NOT NULL,
  tag_id NUMBER(20) NOT NULL,
  CONSTRAINT news_tag_news FOREIGN KEY(news_id) REFERENCES news(news_id),
  CONSTRAINT news_tag_tag FOREIGN KEY(tag_id) REFERENCES tags(tag_id)
);

CREATE SEQUENCE users_id START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE news_id START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE authors_id START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE comments_id START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE tags_id START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE TRIGGER users_insert
BEFORE INSERT ON users
FOR EACH ROW
DECLARE
  num NUMBER;
  curr_id NUMBER(20);
BEGIN
  IF :new.user_id IS NULL THEN
    curr_id := users_id.nextval;
    SELECT COUNT(*) INTO num FROM users WHERE user_id = curr_id;
    IF num <> 0 THEN
      SELECT MAX(user_id) + 1 INTO curr_id FROM users;
    END IF;
    SELECT curr_id INTO :new.user_id FROM dual;
  END IF;
END;

CREATE OR REPLACE TRIGGER news_insert
BEFORE INSERT ON news
FOR EACH ROW
DECLARE
  num NUMBER;
  curr_id NUMBER(20);
BEGIN
  IF :new.news_id IS NULL THEN
    curr_id := news_id.nextval;
    SELECT COUNT(*) INTO num FROM news WHERE news_id = curr_id;
    IF num <> 0 THEN
      SELECT MAX(news_id) + 1 INTO curr_id FROM news;
    END IF;
    SELECT curr_id INTO :new.news_id FROM dual;
  END IF;
END;

CREATE OR REPLACE TRIGGER author_insert
BEFORE INSERT ON authors
FOR EACH ROW
BEGIN
  IF :new.author_id IS NULL THEN
    SELECT authors_id.nextval INTO :new.author_id FROM dual;
  END IF;
END;

CREATE OR REPLACE TRIGGER comments_insert
BEFORE INSERT ON comments
FOR EACH ROW
BEGIN
  IF :new.comment_id IS NULL THEN
    SELECT comments_id.nextval INTO :new.comment_id FROM dual;
  END IF;
END;

CREATE OR REPLACE TRIGGER tag_insert
BEFORE INSERT ON tags
FOR EACH ROW
BEGIN
  IF :new.tag_id IS NULL THEN
    SELECT tags_id.nextval INTO :new.tag_id FROM dual;
  END IF;
END;