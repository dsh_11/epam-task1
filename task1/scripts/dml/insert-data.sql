-------------------------news

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Dash cam footage catches', 
'A driving instructor filmed an unsuspecting individual who failed to stop at a set of traffic',
'Dash cameras have caught countless drivers performing aggressive and dangerous maneuvers but a driving instructor from Reading managed to film a rather funny incident.

Phil Goodall, who lives in Reading town centre, was teaching one of his students on Thursday, February 4 when he saw an unsuspecting individual walk straight into a traffic light post in Basingstoke Road, Reading.

The driving instructor originally bought the dash cam to catch inconsideration motorists who deliberately cut up learner drivers, but he admits that this collision made him chuckle.

Mr Goodall said: "This was just really funny. It made me laugh when he tried to style it out afterwards like nothing happened.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Woman suffers after collision',
'The Reading Buses service 33 collided with a car in Tilehurst Road on Tuesday, February 9',
'A Reading bus and a car collided this afternoon in Tilehurst Road leaving one person in hospital with head injuries.

The accident is thought to have happened near the junction of Water Road and Tilehurst Road at around 1.10pm, Tuesday, February 9.

Spokesman for the South Central Ambulance Service, David Gallagher said: "We sent one rapid response vehicle.

"There was one patient, possibly on the bus, who had minor head injuries."

Reading Buses spokesman Jake Osman said: "One of our buses was involved in a collision, the 33."

Mr Osman added the collision caused a lady to fall on the bus who was later treated by paramedics.

He confirmed the company would be conducting its own investigation into what happened, interviewing the driver and passengers.

Gareth Ford-Lloyd a spokesman for Thames Valley Police said: "We did attend a minor road traffic collision between a car and a bus.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Woodley ballerina secures the',
'Lucy White from Woodley has been selected to perform in this April''s London Children''s Ballet',
'A young prima ballerina from Woodley has impressed at auditions i to be given the chance to perform in the annual London Children''s Ballet performance.

Nine-year-old Lucy White of Arundel Road was one of just 60 children who received a call back for the charity performance of Little Lord Fauntleroy which will be performed in April this year.

Lucy has been dancing since the age of four, she was taught at JG Dance in Woodley and started at Redroofs Performing Arts School, Maidenhead in September.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Bracknell Forest Council budge',
'Here are six things we''ve learnt about the newest Bracknell Forest Council budget',
'The budget for the next 12 months is set to be debated next week by Bracknell Forest Council and the executive committee.

Council leader Paul Bettison and Cllr Peter Heydon talked through the budget have revealed more about what the plan means for the people of Bracknell.

Here are a few things we learnt',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Australia town consumed by',
'A fast-growing tumbleweed called "hairy panic" is clogging up homes in a small Australian town.',
'Extremely dry conditions mean the weeds pile up each day outside a row of homes at Wangaratta, in Victoria''s northeast.

Frustrated residents are forced to clear out the weeds for several hours every day, with piles of hairy panic at times reaching roof height.

A nearby farmer is being blamed for failing to tend to his paddock.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Tiny snail ''swims like a bee''',
'A tiny species of sea snail "flies" underwater using movements like winged insects.',
'US scientists observed the so-called sea butterfly - actually an aquatic snail - using high-speed video and flow-tracking systems.

The 3mm critter flaps its wing structures, which grow where a snail''s foot would normally be, in a characteristic figure-of-eight pattern.

It also uses some of the vortex-making tricks that keep insects in the air.

"It looks like it''s flying, like a very small insect," said Dr David Murphy, a mechanical engineer at Johns Hopkins University.

The study, published in the Journal of Experimental Biology, was part of his PhD research while studying at Georgia Tech.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Apple backed by giants in FBI',
'More of the biggest names in tech - including eBay, Google and Amazon have joined Twitter and AirBnB',
'The FBI has a court order demanding Apple helps unlock an iPhone used by the gunman behind the San Bernardino terror attack, Syed Rizwan Farook.

Farook and his wife killed 14 people in the California city last December before police fatally shot them.

Family members of some victims have backed the FBI''s order.

Two groups of tech giants have now filed an amicus brief, which allows parties not directly involved in a court case, but who feel they are affected by it, to give their view.

Apple has appealed against the court order, arguing that it should not be forced to weaken the security of its own products.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Rayo Vallecano 1-5 Barcelona',
'Barcelona broke record by going 35 games unbeaten after thrashing Rayo Vallecano.',
'Lionel Messi scored a hat-trick as the hosts fell apart after Ivan Rakitic''s opener, a result of a goalkeeper error.

Diego Llorente was sent off in between two Messi goals before Manucho replied.

Manuel Iturra then saw red but Luis Suarez had the resulting penalty saved as Messi wrapped up a 35th Barca treble before Arda Turan completed the rout.

It was the Turkish midfielder''s first Barcelona goal since his move from Atletico Madrid last summer, but in an action-packed game, it was relegated to a mere footnote.

Luis Enrique''s team now sit eight points clear of second-placed Atletico and they could have easily made it even more convincing against Rayo, who are now separated from the relegation zone by goal difference.

Barcelona twice hit the woodwork with Neymar''s free-kick against the bar falling to Sergio Busquets, who was brought down for Rayo''s second red card and a penalty.

Rayo goalkeeper Juan Carlos Martin Corral made up for his earlier mistake by saving Suarez''s spot-kick with Barcelona now having missed half of their 18 penalties this season. That also includes the one taken by Messi when he passed to Suarez.

The hosts will argue that Llorente''s dismissal was harsh as he took the ball despite a high tackle on Rakitic, but they were outclassed as Messi drew level with Real Madrid''s Spanish record-holder Cristiano Ronaldo on 35 hat-tricks.

The Argentine also took his season''s tally to 33 goals.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Rashford ''in contention'' Euro',
'England manager Roy Hodgson has left the door open for Marcus Rashford to make his Euro 2016 squad.',
'The Manchester United striker, 18, scored twice on his debut against FC Midtjylland and then two more in the win over Arsenal on Sunday.

Rashford, who did not score against Watford on Wednesday, has only been capped at under-18 level for England.

"I would neither rule him in or rule him out, I just hope he can do well," Hodgson said.

"I have been watching Rashford for two years, so I have known about him for a long time. He is in our system."

He added he was delighted that an England Under-18 player was getting chances in Manchester United''s first team.

"Most of all, I hope he is allowed to develop as an 18-year-old should and people don''t try to put him under enormous pressure," he said.

"Scoring four goals in three games is a great achievement and if he can keep that up, it is great for Manchester United and England."

Rashford came into Louis van Gaal''s side with fellow strikers Wayne Rooney and Anthony Martial injured but looks set to remain a part of the first-team picture at Old Trafford for the remainder of the season.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('North Korean leader nuclear',
'Kim Jong-un has said North Korea''s nuclear weapons should be ready for use "at any time".',
'He told military leaders North Korea would revise its military posture to be ready to launch pre-emptive strikes, the Korean Central News Agency said.

But despite its rhetoric it remains unclear how advanced the North''s nuclear weapons programme is.

The UN has imposed some of its toughest ever sanctions on the North following its nuclear test and missile launch.

In response on Thursday, the North fired six short-range projectiles into the sea. ',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Scientists ''find cancer''s Achi',
'Scientists believe they have discovered a way to "steer" the immune system to kill cancers.',
'Researchers at University College, London have developed a way of finding unique markings within a tumour - its "Achilles heel" - allowing the body to target the disease.

But the personalised method, reported in Science journal, would be expensive and has not yet been tried in patients.

Experts said the idea made sense but could be more complicated in reality.

However, the researchers, whose work was funded by Cancer Research UK, believe their discovery could form the backbone of new treatments and hope to test it in patients within two years.

They believe by analysing the DNA, they''ll be able to develop bespoke treatment.

People have tried to steer the immune system to kill tumours before, but cancer vaccines have largely flopped.

One explanation is that they are training the body''s own defences to go after the wrong target.

The problem is cancers are not made up of identical cells - they are a heavily mutated, genetic mess and samples at different sites within a tumour can look and behave very differently.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Indian state where a plane',
'Photographer has been fascinated with the variety of sculptures that sit atop roofs of houses.',
'Mr Vora travelled more than 6,000km (3,728 miles) across four districts in the state to photograph the sculptures, introduced to Punjab by immigrants who began building homes in their villages back in the late 1970s.

The sculptures often represent the aspirations of their owners, along with some elements of their personal history, which is why aeroplanes were often a common theme.

The photographs are being displayed in an exhibition called "Everyday Baroque", organised by Delhi-based Photoink.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Abandoned �alien� forts off',
'These sea forts were built to defend the country from air raids but today they look like a lost set',
'If you venture just seven miles off the British coast to a place called Red Sands, you will encounter an unusual sight � giant towers rising from the ocean on rusted legs. They look almost alien, but are actually a relic of Britain�s military past: the Maunsell Forts.

Built in the Thames Estuary to defend the United Kingdom from air raids during the World War Two, the forts have been decommissioned for more than 50 years. Delicate and at risk, they are usually off-limits to the public. But we got special access with the help of a group of passionate volunteers, flying a drone around the strange structures, learning about their surprising history� and seeing the ambitious project trying to secure their future.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Day hackers hijacked my phone',
'Something wasn''t right about the phone screen. It looked odd. And it was then that he saw message.',
'His smart phone had, in effect, been hijacked and he was being asked to pay a ransom.

"My device had been locked", he told BBC Radio 4''s PM Programme.

He was being instructed "to send some money via a voucher code to get the phone unlocked".

Ken Main knows how Nick felt. It happened to him with the computers at his hairdressing salon in Glasgow.

"It was a pale blue screen with the message right in the middle," he explained. "If we wanted the information back we would have to pay a ransom."',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Hubble sets new cosmic record',
'The Hubble Space Telescope has spied the most distant galaxy yet.',
'It is so far away that the light from this extremely faint collection of stars, catalogued as GN-z11, has taken some 13.4 billion years to reach us.

Or to put that another way - Hubble sees the galaxy as it was just 400 million years after the Big Bang.

Astronomers say they are confident about the measurement because they have been able to tease apart and analyse the object''s light.

Such spectroscopic assessments are difficult to perform on the most far-flung sources, but if it can be done it produces the most reliable distance estimates.

The details of the discovery will appear shortly in an edition of the Astrophysical Journal.

"This really represents the pinnacle of Hubble''s exploration of galaxies across cosmic history," said Yale University, US, astronomer Pascal Oesch, the lead author on the study.

"Hubble has proven once again, even after almost 26 years in space, just how special it is.

"When the telescope was launched we were investigating galaxies a little over half-way back in cosmic history. Now, we''re going 97% of the way back. It really is a tremendous achievement," he told BBC News.

Nonetheless, scientists believe they are now at the very limits of what the veteran observatory can achieve technically, and it will most probably fall to Hubble''s successor to look even deeper into space.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Plan to save giant pelicans',
'Dalmatian pelicans are the largest species of pelican. But one of their colonies is under threat',
'Skadar Lake is the biggest freshwater body in the Balkans. Shared between Montenegro and Albania, it hosts the westernmost breeding colony of the endangered Dalmatian pelican.

This colony has struggled for decades due to the floods and human disturbance that were severely affecting the breeding success of the birds.

The team''s actions included: creating floating rafts for the pelicans to nest on, as a way to mitigate the effect of flooding; increasing ranger patrols to avoid disturbance of the colony during the breeding season; and educating local fishermen to strengthen their sense of ownership and responsibility for the lake and the pelicans.

This film was shot by the team of The Living Med, an initiative that aims to further biodiversity conservation in the Mediterranean Basin by using visual storytelling to raise awareness about its unique values and threats.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Five places youre not to visit',
'These secretive places are off-limits to even the most curious travellers',
'The most alluring things in life are often those that are off-limits. And as travellers, this means that we�re fascinated by places that we�re not allowed to visit. To locate some of these oh-so-tantalizing wonders, we turned to question-and-answer site Quora.

From an isolated Pacific island without running water to a James Bond-like spy base in England, here are five of the world�s most secretive places.

Svalbard Global Seed Vault, Norway

Imagine a world forever changed by a massive natural disaster: Earth�s population ravaged, its infrastructure devastated, its food sources decimated. A lone survivor�s mission: to rebuild. 

In such a Hollywood-inspired apocalyptic scenario, where does a hero go? Enter the Svalbard Global Seed Vault in Norway, humanity�s last stand against a global food crisis.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('City of 187 different countrie',
'More than 40% of residents come from somewhere else',
'A haven for expats and loved by locals, Geneva attracts people from around the world for its economic opportunity, lakeside beauty and Mont Blanc views. Home to more international organisations � such as the Red Cross and the United Nations � than any other place in the world, the city has a decidedly global feel. More than 40% of residents are from outside Switzerland, with 187 different countries represented in the mix.

Despite the international influences, Geneva still lays claim to its own particular character and history. In the 16th Century, John Calvin, a major Protestant reformer, gave many of his revolutionary sermons here. Geneva-born Henri Dunant, a follower of the Calvinist faith, helped found the Red Cross in Geneva in 1863, and these humanitarian ideas inspired the first Geneva Convention in 1864.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Chilling at the UK''s station',
' A taste of life on the British Antarctic Survey''s isolated Halley Research Station.',
'"Everybody who goes south is running away from something, Matthew."

Up on the bridge of the Royal Research Ship Ernest Shackleton, 2nd Mate Duncan Robb gives me a meaningful look as he delivers this slice of cynicism, and nudges a lever forward to guide the 5,000-tonne ship towards an ice floe.

With a crunch and a shudder, the strengthened hull effortlessly splits the floe, and elbows the ice aside as we plough onward through sub-zero-temperature water towards a flat horizon.

I didn''t feel as though I was running away. It was more a case of being drawn inexorably back to a place I had visited, all too briefly, once before.

Two years ago I was getting ready for an assignment in Qatar. My schedule was sorted, my interviewees were primed. Then my editor phoned.',
CURRENT_TIMESTAMP, SYSDATE);

INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES('Moments of joy ''damage heart''',
'The emotional stress that causes chest pains and breathlessness can occur in moments of joy as well.',
'Three-quarters of cases of takotsubo cardiomyopathy, a change in the shape of the heart''s left ventricle, which can be fatal, are caused by stress.

The University Hospital Zurich study, in the European Heart Journal, suggests about one in 20 cases is caused by joy.

The condition is normally temporary and people are generally fine afterwards.

In the study of 1,750 patients, researchers discovered heart problems caused by:

    a birthday party
    a son''s wedding
    meeting a friend after 50 years
    becoming a grandmother
    a favourite rugby team winning a game
    winning a casino jackpot
    a computerised tomography (CT) scan giving the all-clear from another condition

The study also suggested most cases were in post-menopausal women.

Dr Jelena Ghadri, one of the researchers, said: "We have shown that the triggers for takotsubo syndrome can be more varied than previously thought.

"A takotsubo syndrome patient is no longer the classic ''broken-hearted'' patient, and the disease can be preceded by positive emotions too.

"Clinicians should be aware of this and also consider that patients who arrive in the emergency department with signs of heart attacks, such as chest pain and breathlessness, but after a happy event or emotion, could be suffering from takotsubo syndrome just as much as a similar patient presenting after a negative emotional event." ',
CURRENT_TIMESTAMP, SYSDATE);


-------------------------author

INSERT INTO authors(author_name) VALUES('Michael Ellis');
INSERT INTO authors(author_name) VALUES('Luca Hall');
INSERT INTO authors(author_name) VALUES('Luca Perry');
INSERT INTO authors(author_name) VALUES('Liam Mitchell');
INSERT INTO authors(author_name) VALUES('Leon Foster');
INSERT INTO authors(author_name) VALUES('Shawn Hendrix');
INSERT INTO authors(author_name) VALUES('Cade Hammond');
INSERT INTO authors(author_name) VALUES('Jagger Nunez');
INSERT INTO authors(author_name) VALUES('Joziah Holcomb');
INSERT INTO authors(author_name) VALUES('Carson Newman');
INSERT INTO authors(author_name) VALUES('Carmen Rose');
INSERT INTO authors(author_name) VALUES('Hayden Moss');
INSERT INTO authors(author_name) VALUES('Rory John');
INSERT INTO authors(author_name) VALUES('Skye Cooke');
INSERT INTO authors(author_name) VALUES('Jody Davies');
INSERT INTO authors(author_name) VALUES('Ashley Juarez');
INSERT INTO authors(author_name) VALUES('Skye Barron');
INSERT INTO authors(author_name) VALUES('Harper Delgado');
INSERT INTO authors(author_name) VALUES('Lee Heath');
INSERT INTO authors(author_name) VALUES('Jess Ratliff');

-------------------------tag

INSERT INTO tags(tag_name) VALUES('Business');
INSERT INTO tags(tag_name) VALUES('Entertainment');
INSERT INTO tags(tag_name) VALUES('Health');
INSERT INTO tags(tag_name) VALUES('National');
INSERT INTO tags(tag_name) VALUES('Politics');
INSERT INTO tags(tag_name) VALUES('World');
INSERT INTO tags(tag_name) VALUES('Nature');
INSERT INTO tags(tag_name) VALUES('Travel');
INSERT INTO tags(tag_name) VALUES('Science');
INSERT INTO tags(tag_name) VALUES('IT');
INSERT INTO tags(tag_name) VALUES('Sport');
INSERT INTO tags(tag_name) VALUES('Medicine');
INSERT INTO tags(tag_name) VALUES('Criminal');
INSERT INTO tags(tag_name) VALUES('Music');
INSERT INTO tags(tag_name) VALUES('Architecture');
INSERT INTO tags(tag_name) VALUES('Education');
INSERT INTO tags(tag_name) VALUES('Accident');
INSERT INTO tags(tag_name) VALUES('');
INSERT INTO tags(tag_name) VALUES('');
INSERT INTO tags(tag_name) VALUES('');

-------------------------news_author

INSERT INTO news_authors(news_id, author_id) VALUES(1, 3);
INSERT INTO news_authors(news_id, author_id) VALUES(2, 6);

-------------------------news_tag

INSERT INTO news_tags(news_id, tag_id) VALUES(1, 1);
INSERT INTO news_tags(news_id, tag_id) VALUES(1, 4);
INSERT INTO news_tags(news_id, tag_id) VALUES(1, 5);
INSERT INTO news_tags(news_id, tag_id) VALUES(2, 1);
INSERT INTO news_tags(news_id, tag_id) VALUES(2, 3);

-------------------------comments

INSERT INTO comments(news_id, comment_text, creation_date) VALUES(2, 'comment1', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date) VALUES(1, 'comment2', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date) VALUES(2, 'comment3', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date) VALUES(1, 'comment4', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date) VALUES(1, 'comment5', CURRENT_TIMESTAMP);